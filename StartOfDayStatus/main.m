/**
 
 OmniFocusInfoScripts
 
 StartOfDayStatus
 
 <br/>
 
 Calculate and build the start of the day status message. Formatted to fit into a single tweet. Example:
 
 Seth's start of day status is 9 tasks for today, 38 tasks for this week, 43 tasks for next week, and 23 overdue tasks.

 and consists of

 1. Tasks for "today" = tasks that are due on today's date and tasks that are marked as @today in their notes
 2. Tasks for this week = tasks for today + flagged tasks
 3. Tasks for next week = tasks that are marked as @nextweek in their notes
 4. Tasks that are overdue and active
 
 <br/>
 
 Usage format:
  StartOfDayStatus --db=... --user=... (or omit command line arguments to use stored preferences or defaults
 
 <br/>
 
 @author <a href="mailto:ofis@homeforderangedscientists.net">Seth Landsman, Ph.D.</a>
 @version 1.0
 */

#import <Foundation/Foundation.h>

#import <OmniFocusAnalyticsFramework/OmniFocusAnalyticsFramework.h>
#import "ArgumentParser.h"
#import "OFHelper.h"

/**
 The notes field tag denoting a task for today
 */
NSString *todayTag()
{
    return @"@today";
}

/**
 The notes field tag denoting a task for next week
 */
NSString *nextWeekTag()
{
    return @"@nextweek";
}

int main(int argc, const char * argv[])
{
    
    @autoreleasepool {
        [OFHelper registerDefaults];
        
        // grab arguments
        NSArray *arguments = [[NSProcessInfo processInfo] arguments];
        NSDictionary *args = [ArgumentParser parseArguments:arguments];
        
        NSString *db = [OFHelper database:args];
        NSString *user = [OFHelper username:args];
        
        OFManager *ofm = [OFHelper initializeSystem:db];

        OFTaskFilter *tf = [[OFTaskFilter alloc] initWithTasks:[[ofm tasks] allValues] andManager:ofm];
        
        // basic filters
        
        NSArray *incompleteTasks = [tf filterIncompleteTasks];
        OFTaskFilter *incompleteTaskFilter =
            [[OFTaskFilter alloc] initWithTasks:incompleteTasks andManager:ofm];
        
        // next level down

        NSArray *markedTodayTasks = [incompleteTaskFilter filterTasksMarkedWithTag:todayTag()];
        NSArray *markedNextWeekTasks = [incompleteTaskFilter filterTasksMarkedWithTag:nextWeekTag()];
        NSArray *flaggedTasks = [incompleteTaskFilter filterFlaggedTasks];
        NSArray *overdueTasks = [incompleteTaskFilter filterOverdueTasks];
        NSArray *dueTodayTasks = [incompleteTaskFilter filterTasksDueWithinMinutes:(24*60*60)];
                
        // final tallies
        
        int nTodayTasks = (int)([markedTodayTasks count] + [dueTodayTasks count]);
        int nThisWeekTasks = (int)(nTodayTasks + [flaggedTasks count]);
        int nNextWeekTasks = (int)([markedNextWeekTasks count]);
        int nOverdueTasks = (int)([overdueTasks count]);
        
        NSString *txt = [NSString stringWithFormat:
                         @"%@'s start of day status is %i tasks for today, %i tasks for this week, %i tasks for next week, and %i overdue tasks.", user, nTodayTasks, nThisWeekTasks, nNextWeekTasks, nOverdueTasks];
        printf("%s\n", [txt UTF8String]);
    }
    return 0;
}
