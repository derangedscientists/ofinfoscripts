// ArgumentParser
// @see ArgumentParser.h

#import "ArgumentParser.h"

@implementation ArgumentParser

+(NSDictionary *)parseArguments:(NSArray *)args
{
    NSMutableDictionary *pargs = [[NSMutableDictionary alloc] init];
    NSEnumerator *aen = [args objectEnumerator];
    NSString *arg;
    
    NSString *arg_prefix = @"--";
    
    while (arg = [aen nextObject])
    {
        if ([arg hasPrefix:arg_prefix])
        {
            arg = [arg substringFromIndex:[arg_prefix length]];
            if ([arg rangeOfString:@"="].location == NSNotFound)
            {
                [pargs setObject:@"YES" forKey:arg];
            } else
            {
                NSArray *c = [arg componentsSeparatedByString:@"="];
                [pargs setObject:[c objectAtIndex:1] forKey:[c objectAtIndex:0]];
            }
        }
    }
    return pargs;
}

@end
