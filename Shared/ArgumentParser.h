/**
 
 OmniFocusInfoScripts
 
 ArgumentParser
 
 <br/>
 
 The argument parser is a dirt simple command line args parser. Looks for command lines in the form of --foo=bar and 
    --baz. Results in an NSDictionary containing {{foo, bar}, {baz, YES}}, all in the forms of NSStrings.
 
 <br/>
 
 @author <a href="mailto:ofis@homeforderangedscientists.net">Seth Landsman, Ph.D.</a>
 @version 1.0
 */

#import <Foundation/Foundation.h>

@interface ArgumentParser : NSObject

/**
 Parse array of strings represents command line arguments and returns an NSDictionary as per the header comments
 */
+(NSDictionary *)parseArguments:(NSArray *)args;

@end
