// OFHelper
// @see OFHelper.h

#import "OFHelper.h"
#import "OmniFocusAnalyticsFramework/OmniFocusAnalyticsFramework.h"

@implementation OFHelper

+(void)registerDefaults
{
    NSMutableDictionary *defaults = [[NSMutableDictionary alloc] init];
    
    NSString *db = [[NSString alloc]
                    initWithFormat:@"%@/Library/Caches/com.omnigroup.OmniFocus/OmniFocusDatabase2",
                    NSHomeDirectory()];
    [defaults setObject:db forKey:@"database"];
    
    NSString *uname = NSUserName();
    [defaults setObject:uname forKey:@"user"];
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaults];
}

+(NSString *)database:(NSDictionary *)args
{
    // check if there is a db argument
    NSString *db = [args objectForKey:@"db"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (db != NULL)
    {
        // if db does exist,
        bool flag = [args objectForKey:@"test"];
        if (!flag)
        {
            // update defaults
            [defaults setValue:db forKey:@"database"];
            [defaults synchronize];
        }
    } else
    {
        // pull from defaults
        db = [defaults stringForKey:@"database"];
    }
    
    return db;
}

+(NSString *)username:(NSDictionary *)args
{
    // check if there is a db argument
    NSString *uname = [args objectForKey:@"user"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (uname != NULL)
    {
        // if db does exist,
        bool flag = [args objectForKey:@"test"];
        if (!flag)
        {
            // update defaults
            [defaults setValue:uname forKey:@"user"];
            [defaults synchronize];
        }
    } else
    {
        // pull from defaults
        uname = [defaults stringForKey:@"user"];
    }
    
    return uname;
}

+(OFManager *)initializeSystem:(NSString *)ofdb
{
    NSLog(@"OF database is %@", ofdb);
    OFManager *ofm = [[OFManager alloc] initWithDatabase:ofdb];
    NSLog(@"Initiailized manager %@", ofm);
    
    return ofm;
}

@end
