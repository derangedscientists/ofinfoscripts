/**
 
 OmniFocusInfoScripts
 
 OFHelper
 
 <br/>
 
 This class handles the various start up techniques associated with an OmniFocus Info Script
 
 Generally, the following actions should be executed when building a new info script:
 
 <code>
    [OFHelper registerDefaults];
 
    // grab arguments
    NSArray *arguments = [[NSProcessInfo processInfo] arguments];
    NSDictionary *args = [ArgumentParser parseArguments:arguments];
    NSLog(@"Parsed args to %@", args);
 
    NSString *db = [OFHelper database:args];
    NSString *user = [OFHelper username:args];
 
    OFManager *ofm = [OFHelper initializeSystem:db];
 </code>
 
 <br/>
 
 @author <a href="mailto:ofis@homeforderangedscientists.net">Seth Landsman, Ph.D.</a>
 @version 1.0
 */

#import <Foundation/Foundation.h>
#import "OmniFocusAnalyticsFramework/OmniFocusAnalyticsFramework.h"

@interface OFHelper : NSObject

/**
 Register NSUserDefaults for saving preferences for database and user
 
 Requires override or replacement if there are more defaults beyond just database
 and user
 */
+(void)registerDefaults;

/**
 Get the OmniFocus database location from a combination of
 the command line, NSUserDefaults, and meaningful defaults (in that order)
 
 Writes new defaults unless the --test flag is set on the command line
 
 Looks for the "db" command line arg
 
 Default is calculated in registerDefaults as 
 
 initWithFormat:@"%@/Library/Caches/com.omnigroup.OmniFocus/OmniFocusDatabase2",
 NSHomeDirectory()];
 
 Takes as args the output of [ArgumentParser parseArguments]
*/
+(NSString *)database:(NSDictionary *)args;

/**
 Get the user name from a combination of
 the command line, NSUserDefaults, and meaningful defaults (in that order)
 
 Writes new defaults unless the --test flag is set on the command line
 
 Looks for the "user" command line arg
 
 Default is calculated in registerDefaults as NSUserName();
 
 Takes as args the output of [ArgumentParser parseArguments]
 */
+(NSString *)username:(NSDictionary *)args;

/**
 Initialize the OmniFocus Manager object with the database. Returns the OFM to be used
 throughout the life of the info script
 */
+(OFManager *)initializeSystem:(NSString *)ofdb;

@end
