/**
 
 OmniFocusInfoScripts
 
 EndOfDayStatus
 
 <br/>
 
 Calculate and build the end of the day status message. Formatted to fit into a single tweet. Example:
 
 Seth's end of day status is 81 active projects, 1 tasks done today, 0 tasks added today, 167 open tasks, 20 waiting fors

 and consists of:
 
 1. Active projects, which means active projects at least one task
 2. Tasks with a completed date of today
 3. Tasks with an added date of today
 4. Number of active, incomplete, non-repeating tasks
 5. Number of waiting for tasks
 
 <br/>
 
 Usage format:
 EndOfDayStatus --db=... --user=... (or omit command line arguments to use stored preferences or defaults
 
 <br/>
 
 @author <a href="mailto:ofis@homeforderangedscientists.net">Seth Landsman, Ph.D.</a>
 @version 1.0
 */

#import <Foundation/Foundation.h>
#import <OmniFocusAnalyticsFramework/OmniFocusAnalyticsFramework.h>
#import "ArgumentParser.h"
#import "OFHelper.h"

/**
 The notes field tag denoting a task for today
 */
NSString *todayTag()
{
    return @"@today";
}

/**
 The notes field tag denoting a task for next week
 */
NSString *nextWeekTag()
{
    return @"@nextWeek";
}

int main(int argc, const char * argv[])
{
    
    @autoreleasepool {
        [OFHelper registerDefaults];
        
        // grab arguments
        NSArray *arguments = [[NSProcessInfo processInfo] arguments];
        NSDictionary *args = [ArgumentParser parseArguments:arguments];
        NSLog(@"Parsed args to %@", args);
        
        NSString *db = [OFHelper database:args];
        NSString *user = [OFHelper username:args];
        
        OFManager *ofm = [OFHelper initializeSystem:db];

        OFTaskFilter *tf = [[OFTaskFilter alloc] initWithTasks:[[ofm tasks] allValues] andManager:ofm];
        OFProjectFilter *pf = [[OFProjectFilter alloc] initWithProjects:[[ofm projects] allValues] andManager:ofm];
        
        // basic filters
        
        NSArray *activeTasks = [tf filterActiveTasks];
        OFTaskFilter *activeTaskFilter = [[OFTaskFilter alloc] initWithTasks:activeTasks andManager:ofm];

        NSArray *incompleteTasks = [tf filterIncompleteTasks];
        OFTaskFilter *incompleteTaskFilter = [[OFTaskFilter alloc] initWithTasks:incompleteTasks andManager:ofm];
        
        NSArray *activeIncompleteTasks = [activeTaskFilter filterIncompleteTasks];
        OFTaskFilter *activeIncompleteTaskFilter = [[OFTaskFilter alloc] initWithTasks:activeIncompleteTasks andManager:ofm];
        
        NSArray *completedTasks = [tf filterCompletedTasks];
        OFTaskFilter *completeTaskFilter = [[OFTaskFilter alloc] initWithTasks:completedTasks andManager:ofm];
        
        NSArray *activeProjects = [pf filterActiveProjects];
        OFProjectFilter *activeProjectsFilter = [[OFProjectFilter alloc] initWithProjects:activeProjects andManager:ofm];
        
        // next level down

        NSArray *nonEmptyActiveProjects = [activeProjectsFilter filterNonEmptyProjects];;
        NSArray *waitingForTasks = [incompleteTaskFilter filterInContextTasks:@"Waiting For"];
        NSArray *activeIncompleteNonRepeatingTasks = [activeIncompleteTaskFilter filterNonRepeatingTasks];
        OFTaskFilter *activeIncompleteNonRepeatingTaskFilter = [[OFTaskFilter alloc] initWithTasks:activeIncompleteNonRepeatingTasks andManager:ofm];
        NSArray *nonWaitingNonRepeatingActiveIncompleteTasks = [activeIncompleteNonRepeatingTaskFilter filterNotInContextTasks:@"Waiting For"];
        NSArray *nonRepeatingCompletedTasks = [completeTaskFilter filterNonRepeatingTasks];
        OFTaskFilter *nonRepeatingCompletedTaskFilter = [[OFTaskFilter alloc] initWithTasks:nonRepeatingCompletedTasks andManager:ofm];
        OFTaskFilter *nonWaitingNonRepeatingActiveIncompleteTaskFilter = [[OFTaskFilter alloc] initWithTasks:nonWaitingNonRepeatingActiveIncompleteTasks andManager:ofm];
        
        NSArray *completedTodayTasks = [nonRepeatingCompletedTaskFilter filterTasksCompletedWithinMinutes:(60*60*24)];
        NSArray *addedTodayTasks = [nonWaitingNonRepeatingActiveIncompleteTaskFilter filterTasksAddedWithinMinutes:(60*60*24)];
        
        // final tallies
        
        int nActiveProjects = (int)[nonEmptyActiveProjects count];
        int nWaitingFor = (int)[waitingForTasks count];
        int nOpenTasks = (int)[nonWaitingNonRepeatingActiveIncompleteTasks count];
        int nCompletedToday = (int)[completedTodayTasks count];
        int nAddedToday = (int)[addedTodayTasks count];
        
        NSString *txt = [NSString stringWithFormat:
                         @"%@'s end of day status is %i active projects, %i tasks done today, %i tasks added today, %i open tasks, %i waiting fors",
                         user, nActiveProjects, nCompletedToday, nAddedToday, nOpenTasks, nWaitingFor];
        printf("%s\n", [txt UTF8String]);        
    }
    return 0;
}
