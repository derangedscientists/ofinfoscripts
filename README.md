#OmniFocusInfoScripts#

These scripts leverage the OmniFocusAnalyticsFramework (https://bitbucket.org/slandsman/ofanalyticsframework) to
calculate a simple start and end of day state of task list. These statements are designed to be good twitter tweets
and should provide a jumping off point, should someone want to customize or otherwise leverage the framework.

##Usage##

Using the package, the framework will be installed in /Library/Frameworks and the status binaries in /usr/bin (though use 
of the installer is entirely optional, the OS just needs to find the framework for the binaries.

Running the scripts is done via:

> StartOfDayStatus

which uses the defaults of the logged in user's name and the common location of the OmniFocus database (*$USER/Library/Caches/com.omnigroup.OmniFocus/OmniFocusDatabase2*)

> StartOfDayStatus --db=~/testDatabase.sqlite

which uses the database in the proscribed location and, thereafter, will default to use that database until told otherwise

> StartOfDayStatus --db=~/testDatabase.sqlite --test

which uses the database in the proscribed location for only this execution.

> StartOfDayStatus --user=Bob

which uses Bob as the username and will use it thereafter until told otherwise

The *--test* flag will use the command line arguments only during this execution. Omitted *--test* will use the
command line arguments on each invocation thereafter (as per NSUserDefaults).

##License##

see LICENSE.txt (MIT license)

##Caveats##

The package is not very well tested. It works on my local 10.8 machines, but your milage may vary.

##Other##

Comments, complaints, suggestions, etc: please send to ofis@homeforderangedscientists.net

##Thanks##