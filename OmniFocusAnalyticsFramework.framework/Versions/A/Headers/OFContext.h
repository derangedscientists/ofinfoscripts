//
//  OFContext.h
//  OFInfoScripts
//
//  Created by Seth Landsman on 11/17/12.
//  Copyright (c) 2012 Home For Deranged Scientists. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OFManager.h"

@interface OFContext : NSObject

-(id)initWithManager:(OFManager *)mgr andData:(NSDictionary *)data;
-(NSString *)key;
-(NSString *)parent;
-(NSString *)name;
-(NSArray *)children;
-(void)addChild:(NSString *)key;
-(NSString *)prettyPrintTree;
-(NSString *)fullyQualifiedContext;

@end
