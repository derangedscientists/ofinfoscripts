//
//  OFProject.h
//  OFInfoScripts
//
//  Created by Seth Landsman on 11/17/12.
//  Copyright (c) 2012 Home For Deranged Scientists. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "OFManager.h"
#import "OFTask.h"

@interface OFProject : NSObject

-(id)initWithManager:(OFManager *)mgr Data:(NSDictionary *)data andTask:(OFTask *)task;
-(NSString *)key;
-(NSString *)name;
-(NSArray *)tasks;
-(BOOL)active;
-(NSString *)status;
-(void)addChild:(NSString *)key;

@end
