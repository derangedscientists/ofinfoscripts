//
//  OFTask.h
//  OFInfoScripts
//
//  Created by Seth Landsman on 11/17/12.
//  Copyright (c) 2012 Home For Deranged Scientists. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OFManager.h"

@interface OFTask : NSObject

-(id)initWithManager:(OFManager *)mgr andData:(NSDictionary *)data;
-(NSString *)key;
-(NSString *)project;
-(NSString *)name;
-(NSString *)projectLink;
-(NSString *)notes;
-(NSString *)containingProject;
-(BOOL)flagged;
-(BOOL)complete;
-(BOOL)repeating;
-(NSString *)context;
-(NSDate *)dateDue;
-(NSDate *)dateCompleted;
-(BOOL)overdue;
-(NSDate *)dateAdded;

@end
