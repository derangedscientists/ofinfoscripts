//
//  OFProjectFilter.h
//  OmniFocusAnalyticsFramework
//
//  Created by Seth Landsman on 12/15/12.
//  Copyright (c) 2012 Home For Deranged Scientists. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "OFManager.h"

@interface OFProjectFilter : NSObject

-(id)initWithProjects:(NSArray *)projects andManager:(OFManager *)manager;

-(NSArray *)filterActiveProjects;
-(NSArray *)filterNonEmptyProjects;

@end
