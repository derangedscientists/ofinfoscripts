//
//  OFTaskFilter.h
//  OmniFocusAnalyticsFramework
//
//  Created by Seth Landsman on 12/15/12.
//  Copyright (c) 2012 Home For Deranged Scientists. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "OFManager.h"

@interface OFTaskFilter : NSObject

-(id)initWithTasks:(NSArray *)tasks andManager:(OFManager *)manager;

-(NSArray *)filterActiveTasks;
-(NSArray *)filterIncompleteTasks;
-(NSArray *)filterCompletedTasks;
-(NSArray *)filterNonRepeatingTasks;
-(NSArray *)filterRepeatingTasks;
-(NSArray *)filterTasksDueWithinMinutes:(int)minutes;
-(NSArray *)filterTasksCompletedWithinMinutes:(int)minutes;
-(NSArray *)filterTasksAddedWithinMinutes:(int)minutes;
-(NSArray *)filterTasksMarkedWithTag:(NSString *)tag;
-(NSArray *)filterFlaggedTasks;
-(NSArray *)filterOverdueTasks;
-(NSArray *)filterInContextTasks:(NSString *)ctx;
-(NSArray *)filterNotInContextTasks:(NSString *)ctx;

@end
