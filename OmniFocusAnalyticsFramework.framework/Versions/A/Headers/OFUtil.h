//
//  OFUtil.h
//  OFInfoScripts
//
//  Created by Seth Landsman on 12/7/12.
//  Copyright (c) 2012 Home For Deranged Scientists. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OFUtil : NSObject

+(NSDate *)resolveDate:(double)date;
+(NSDate *)beginningOfToday;
+(NSDate *)endOfToday;
+ (BOOL)date:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate;

@end
