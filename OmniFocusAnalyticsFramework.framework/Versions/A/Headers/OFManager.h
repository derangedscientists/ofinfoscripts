//
//  OFManager.h
//  OFInfoScripts
//
//  Created by Seth Landsman on 11/17/12.
//  Copyright (c) 2012 Home For Deranged Scientists. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OFManager : NSObject

@property NSString *database;

@property NSDictionary *contexts;
@property NSDictionary *topLevelContexts;

@property NSDictionary *projects;
@property NSDictionary *activeProjects;

@property NSDictionary *tasks;
@property NSDictionary *incompleteTasks;

-(id)initWithDatabase:(NSString *)db;
-(id)lookup:(NSString *)key;

@end
